library(tidyverse)
library(patchwork)
library(scales)
library(ComplexHeatmap)

theme_set(theme_bw(base_family = "Helvetica"))

### LOCATIONS
locations_file <- "data/input_data/genomes/Ptrico.gff" 
genome_index <- "data/input_data/genomes/Ptrico.fna.fai"

locations <- rtracklayer::readGFF(locations_file)

chromosomes <- read_delim(genome_index, col_names = c("contig", "end")) %>% 
  mutate(start=1, contig = fct_reorder(contig, end)) %>% 
  select(contig, start, end) 

# blast_results <- read_delim("Adeanei_tax.blast", col_names = col_names)# %>% filter(pident>40, qcovhsp>40)
results <- left_join(hgtec, y = as_tibble(locations), by = c("protein"="protein_id")) %>% 
  mutate(seqid = factor(seqid, levels(chromosomes$contig)))

plot_chroms <- ggplot() +
  geom_segment(aes(x=start, xend=end, y=contig, yend=contig), data = chromosomes,
               size=3, color="grey60") +
  geom_segment(aes(x=start, xend=end, y=seqid, yend=seqid, color=label), 
               data = results, size=3) +
  # facet_grid(vars(hit)) +
  # coord_flip() +
  scale_color_manual(values = col_hgt) +
  theme(legend.position = "bottom")

plot_chroms

