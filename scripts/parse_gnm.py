#!/usr/bin/env python3
import argparse
from glob import glob
import pandas as pd
import string


def parse_args():
    parser=argparse.ArgumentParser(description="Rename Proteomes")
    parser.add_argument('-i', '--input', dest='input', required=True, type=str,
                    help='Genome metadata'),
    parser.add_argument('-d', '--directory', dest='folder', required=True, type=str,
                help='Proteomes directory'),
    parser.add_argument('-o', '--outdir', dest='outdir', required=True, type=str,
            help='Output folder'),
    args=parser.parse_args()
    return args


def clean_seq(seq):
    oseq = seq.replace('J', 'X').replace('B', 'X').replace('O', 'X')
    oseq = oseq.replace('U', 'X').replace('Z', 'X').replace('*', '')

    return oseq

def read_fasta(fafile):
    seqs = {}
    seqid = ''

    for line in open(fafile):
        line = line.replace('\n', '')
        if '>' in line:
            if seqid == '':
                seqid = line.replace('>', '').split(' ', 1)[0]
                s = []
            else:
                seqs[seqid] = clean_seq(''.join(s))
                seqid = line.replace('>', '')
                s = []
        else:
            s.append(line)
    
    if len(s) > 0:
        seqs[seqid] = clean_seq(''.join(s))

    return(seqs)

def write_fasta(seqs, rename=False, taxid=None, taxiddic = None, virus=False, filename=None,
                seqlen=60, ofilenm=None):
    ostr = ''
    if rename:
        i = 0
        j = 0
        k = 0

    for seq in seqs:
        if rename:
            if taxid is not None:
                protid = alph[i] + alph[j] + str(k).zfill(len(str(len(seqs))))
                ostr += ('>%s_%s_%s\n' % (taxid, filename, protid))
            elif virus and taxiddic is not None:
                protid = alph[i] + alph[j] + str(k).zfill(len(str(len(seqs))))
                virus = seq.split('|')[4]
                ostr += ('>%s_%s_%s\n' % (taxiddic[virus], filename, protid))


            j += 1
            if i > len(alph) - 1:
                i = 0
                if j > len(alph) - 1:
                    j = 0
            elif j > len(alph) - 1:
                j = 0
                i += 1
                if i > len(alph) - 1:
                    i = 0
            k += 1

        else:
            ostr += ('>%s\n' % seq)
        for l in range(0, len(seqs[seq]), seqlen):
            ostr += seqs[seq][l:l+seqlen] + '\n'
    
    if ofilenm is not None:
        ofile = open(ofilenm, 'w')
        ofile.write(ostr)
        ofile.close()

    return(ostr)


if __name__ == '__main__':
    inputs=parse_args()

    alph = string.ascii_uppercase

    df = pd.read_csv(inputs.input, sep="\t", header=None, names=["mnemo", "taxid"])
    df['genomeid'] = [x.split('.')[0].replace('_', '') for x in df['mnemo']]
    # df['genomeid'] = [x for x in df['mnemo']]

    taxids = dict(zip(df['genomeid'], df['taxid']))
    print(taxids)
    # eToLDB
    files = glob(inputs.folder+'/*faa')
    odir = inputs.outdir

    for file in files:
        print('Processing: %s' % file, flush=True)
        filenm = ''.join(file.rsplit('/', 1)[1].split('_')[0:2]).split('.')[0]
        print(filenm)
        ofilenm = '%s/%s_parsed.fa' % (odir, filenm)
        seqs = read_fasta(file)
        write_fasta(seqs, True, taxid=taxids[filenm],
                    filename=filenm, ofilenm=ofilenm)

