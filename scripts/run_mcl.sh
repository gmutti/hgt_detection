input=$1
inflation=$2

outdec=$(printf %.0f $(echo "10*$inflation" | bc))

awk '$3>80 && $14>70' $input | cut -f 1,2,11 > seq.abc

mcxload -abc seq.abc --stream-mirror --stream-neg-log10 -stream-tf 'ceil(200)' -o seq.mci -write-tab seq.tab
mcl seq.mci -I $inflation

mcxdump -icl out.seq.mci.I${outdec} -tabr seq.tab -o dump.seq.mci.I${outdec}
