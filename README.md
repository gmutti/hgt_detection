# HGT detection pipeline v.1

# Usage

You need to specify a proteome directory in the config, a blast directory with blast results for each proteome you want to analyse (must have same name of the fasta)

## Add your own (sub)-taxonomy

Insert tutorial

get fastas from taxid
parse sequences
taxonkit create-taxdump -A 1 -x test -O test_v2 all

### Installation

#### Dependencies

* snakemake - conda
* hgtector2 - pip
* taxonkit - conda
* blast suite - conda
* mafft - conda
* proteinortho - conda
* famsa - conda
* seqkit - conda
* trimal - conda
* fasttree - conda
* FastRoot - pip
* tidyverse R package - CRAN
* phytools - CRAN
* ggtree - Bioconductor
* ggtreeExtra - Bioconductor
* Biostrings - Bioconductor
* ComplexHeatmap - Bioconductor

You can use conda or mamba to create this environment, ideally use mamba:

```
mamba create -c conda-forge -c bioconda -n hgt snakemake
mamba activate hgt

# install hgtector
mamba install -c conda-forge pyyaml pandas matplotlib scikit-learn bioconda::diamond
pip install git+https://github.com/qiyunlab/HGTector.git

# install other dependencies
mamba install -c bioconda mafft trimal fasttree taxonkit proteinortho famsa seqkit csvtk

# install R if you need
mamba install -c conda-forge r-base
mamba install -c conda-forge -c bioconda r-tidyverse r-optparse r-magic r-phytools bioconductor-ggtree bioconductor-ggtreeextra bioconductor-complexheatmap  bioconductor-biostrings

# install fastroot
python3 -m pip install FastRoot
fr=$(which FastRoot.py)
dos2unix $fr
```


# TODOs

* add link to deps
* Check and comment why you did AHS2

* stop the alignment if less than x sequences and better handle the weird trees

* UpsetR plot for all indexes
* add QC plots

* mark HGT tree by topological rule
* get df of tree with all different properties so user can filter
* maybe one strategy would be to compute big trees in a fast way then reduce taxonomic redundancy and compute a more accurate tree

* annotate domains and protein functions

* mask proteins and then do dataframe with protein length, %masked proportion to eventually filter downstream results

* option to define "close" outgroup and compute other indexes

* identify potential donors from blast results (recurring weird taxons)
* one day 3 modalities
    * --interdomain: easy mode for proka2euka lgt
    * --donors: user-defined putative donors
    * --reconcile: with small curated database and species tree run reconciliation

* what to do with foldseek?
* matreex plots?

# Done
* pass to diamond
* more elegant way to get sequences to do the tree 