import json
import pandas as pd

configfile: "data/configs/HGT_config.yml"

input_data = pd.read_csv(config['input'], sep='\t')
clades = list(set(input_data['clade']))

mnemo_dict = input_data.set_index('mnemo').T.to_dict()
species = list(mnemo_dict.keys())

# Important dbs and blast directory
# blast=config["blast"]
db=config["db"]
foldseek_db=config["foldseek_db"]

# HGTECTOR FILTERS
max_hits=config["max_hits"]

# BLAST FILTERING PARAMS
evalue=config["evalue"]
len_ratio=config["len_ratio"]
min_cov=config["min_cov"]

# HGT FILTERING PARAMS
min_AI=config["min_AI"]
min_AI2=config["min_AI2"]
min_outpct=config["min_outpct"]

# TREE RECONSTRUCTION PIPELINE PARAMS
n_seqs=config["n_seqs"]

# output directories
output=config["output"]

outfiles = []

for sp in mnemo_dict.keys():
    id_file = output+f"{sp}/ids/{sp}_all.txt"
    outfiles.append(id_file)
    # outfiles.append(output+f"{sp}/plots")
    # this should be optional as some proteomes are not in uniprot and do not have pdbs
    # outfiles.append(output+f"{sp}/pdbs")
    outfiles.append(output+f"{sp}/plots/{sp}_phylo_clusters.pdf")
    outfiles.append(output+f"{sp}/hgt_trees/{sp}_trees.nwk")
    outfiles.append(output+f"{sp}/hgt_trees/{sp}_rooted_trees.nwk")

rooting_methods = ['mad', 'minvar', 'midpoint', 'og']

def get_trees(wildcards):
    checkpoint_output = checkpoints.merge_IDs.get(**wildcards).output.tree_ids
    with open(checkpoint_output) as all_genes:
        seeds = [gn.strip() for gn in all_genes]
    return expand(output+"{sp}/trees/{i}.nwk", sp=wildcards.sp, i=seeds)


def get_rooted_trees(wildcards):
    checkpoint_output = checkpoints.merge_IDs.get(**wildcards).output.tree_ids
    with open(checkpoint_output) as all_genes:
        seeds = [gn.strip() for gn in all_genes]
    return expand(output+"{sp}/rooted_trees/rooted_{i}_{method}.nwk", sp=wildcards.sp, i=seeds, method=rooting_methods)

# def get_tree_plots(wildcards):
#     checkpoint_output = checkpoints.merge_IDs.get(**wildcards).output.tree_ids
#     with open(checkpoint_output) as all_genes:
#         seeds = [gn.strip() for gn in all_genes]
#     return expand(output+"{sp}/tree_plots/{i}.png", sp=wildcards.sp, i=seeds)


rule hgt_hits:
    input:
        outfiles

# ideally compute dbsize for broaddb once its changed with:
# size_db=$(blastdbcmd -metadata -db $db | grep 'number-of-letters' | cut -f2 -d':' | sed 's/,//' | sed 's/ //g')

# rule get_blast:
#     params:
#         broad=lambda wcs: mnemo_dict[wcs.sp]['blast_broaddb'],
#         local=lambda wcs: mnemo_dict[wcs.sp]['blast_local']
#     output:
#         output+"{sp}/blast/{sp}.blast"
#     benchmark:
#         output+"benchmarks/{sp}_getblast.txt"
#     shell:'''
#         echo "concatenating and sorting by query"
#         cat {params.broad} {params.local} | sort -k1 > {output}.temp

#         mkdir $TMPDIR/{wildcards.sp}
#         echo "splitting the files"
#         awk -v tmpdir=$TMPDIR/{wildcards.sp} '{{print>tmpdir"/"$1}}' {output}.temp

#         echo "sorting by bitscore (descending) and taking the first 1000 hits per query"
#         for file in $TMPDIR/{wildcards.sp}/*; do 
#             sort -k12gr $file | awk 'NR<=1000'
#         done > {output}

#         rm {output}.temp
#     '''
        # cat {params.broad} {params.local} | \
        # csvtk  sort -k 1 -k 12:nr -t -H > {output}
        # csvtk  uniq -t -H -f 3,4,5,6,7,8,9,10,11,12,13,14,15,16,17 > {output}



# Selfblast to compute self bitscore and eventually use it to define gene families
# with diamond the e-values were different from blast!
rule selfblast:
    input: lambda wcs: mnemo_dict[wcs.sp]['proteome']
        # proteome+"{sp}.fa"
    output:
        tsv=output+"{sp}/homology/{sp}_self.tsv",
        db=output+"{sp}/homology/{sp}.dmnd"
    threads: 12
    shell:'''
output_pre={output.db}
db_dmnd=$(echo "${{output_pre%.*}}")
diamond makedb --in {input} -d $db_dmnd
diamond blastp -q {input} -d $db_dmnd --out {output.tsv} \
--outfmt 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovhsp qlen slen \
--threads {threads} --evalue 1e-10 --max-hsps 1 --ultra-sensitive
'''
# makeblastdb -parse_seqids -dbtype prot -in {input}
# blastp -query {input} -db {input} \
# -outfmt "6 std qcovs qcovhsp qlen slen staxids" -num_threads {threads} | \
# awk '$11<1e-10' > {output}

# get taxonomy to annotate blast results
rule get_taxonomy:
    input:
        lambda wcs: mnemo_dict[wcs.sp]['blast']
    output:
        output+"{sp}/taxonomy/{sp}_tax.txt"
    params:
        taxdump=lambda wcs: mnemo_dict[wcs.sp]['taxdump']
    # conda:
    #     "data"
    shell:'''
awk 'NR>1' {input} | cut -f2 | cut -f1 -d'_' | sort -u | \
taxonkit reformat --data-dir {params.taxdump} -I1  \
-f \'{{k}}\\t{{p}}\\t{{c}}\\t{{o}}\\t{{f}}\\t{{g}}\\t{{s}}\' > {output}
'''

# Parse blast to compute new indexes and filter it
rule parse_blast:
    input:
        prot=lambda wcs: mnemo_dict[wcs.sp]['proteome'],
        blast=lambda wcs: mnemo_dict[wcs.sp]['blast'],
        tax=rules.get_taxonomy.output,
        selfblast=rules.selfblast.output.tsv
    output:
        output+"{sp}/homology/{sp}_filtered.tsv"
    params:
        seed_id=lambda wcs:mnemo_dict[wcs.sp]['taxid']
    shell:'''
Rscript ./scripts/parse_blast.R -i {input.blast} -t {input.tax}\
 -e {evalue} -c {min_cov} -l {len_ratio}\
 -s {params.seed_id} -a {input.selfblast} -o {output}
'''


# Compute alien indexes
rule compute_HGT_index:
    input:
        rules.parse_blast.output
    output:
        output+"{sp}/homology/{sp}_filtered_AI.tsv"
    shell:'''
Rscript ./scripts/compute_HGT_index.R -i {input} -k Eukaryota -o {output}
'''


# Run hgtector
rule HGTector2:
    input:
        prot=lambda wcs: mnemo_dict[wcs.sp]['proteome'],
        blast=rules.parse_blast.output
    output:
        hgt_blast=output+"{sp}/hgtector/{sp}.blast",
        search=directory(output+"{sp}/hgtector/{sp}_search"),
        analysis=directory(output+"{sp}/hgtector/{sp}_analysis")
    params:
        log=output+"{sp}/hgtector/{sp}_hgtector.log",
        seed_id=lambda wcs: mnemo_dict[wcs.sp]['taxid'],
        tdump=lambda wcs: mnemo_dict[wcs.sp]['taxdump']
    # conda:
    #     "HGT"
    shell:
        '''
awk 'NR>1' {input.blast} | cut -f1,2,3,11,12,13,16 > {output.hgt_blast}
hgtector search -i {input.prot} -m precomp -s {output.hgt_blast} \
-o {output.search} -t {params.tdump} --evalue {evalue} | tee {params.log}

hgtector analyze -i {output.search} -o {output.analysis} \
-t {params.tdump} -k {max_hits} --input-tax {params.seed_id} --outliers none | tee -a {params.log}
'''

rule get_inparalogs:
    input: 
        prot=lambda wcs: mnemo_dict[wcs.sp]['proteome']
    output:
        output+'{sp}/queries/{sp}_inparalogs.tsv'
    shell:'''
outdir=$(dirname {output})
mkdir $TMPDIR/{wildcards.sp}
mmseqs easy-linclust {input.prot} $outdir/{wildcards.sp} $TMPDIR/{wildcards.sp} --min-seq-id 0.9 -c 0.8 --cov-mode 5
csvtk  fold -f 1 -t -H $outdir/{wildcards.sp}_cluster.tsv -v 2 -s',' | cut -f2 | tr ',' '\t' | \
awk '{{print NF"\t"$0}}' | sort -n -k1,1 -r | cut -f2- > {output}
'''

# Merge ids from hgtector and AI
checkpoint merge_IDs:
    input:
        hgtector=rules.HGTector2.output.analysis,
        AI=rules.compute_HGT_index.output,
        paralogs=rules.get_inparalogs.output
    output:
        all_ids=output+"{sp}/ids/{sp}_all.txt",
        tree_ids=output+"{sp}/ids/{sp}_tree.txt",
        both=output+"{sp}/ids/{sp}_both.txt",
        hgtector=output+"{sp}/ids/{sp}_hgtector.txt",
        indexes=output+"{sp}/ids/{sp}_AIs.txt",
        a=temp(output+"{sp}/ids/a_hgt.txt"),
        b=temp(output+"{sp}/ids/b_hgt.txt"),
        # plot=directory(output+"{sp}/plots")
    params:
        blast=output+"{sp}/blast/{sp}_filtered.blast"
    # benchmark:
    #     output+"benchmarks/{sp}_selfblast.txt"
    shell:
        '''
mkdir -p $(dirname {output.all_ids})

cut -f1 {input.hgtector}/hgts/* | sort > {output.a}

csvtk filter2 {input.AI} -t -f'$out_pct_O > {min_outpct} || $AI > {min_AI} || $AI2 > {min_AI2}' | \
awk 'NR>1' | cut -f1 | sort > {output.b}

comm -12 {output.a} {output.b} > {output.both}
comm -23 {output.a} {output.b} > {output.hgtector}
comm -13 {output.a} {output.b} > {output.indexes}

echo "There are $(wc -l < {output.both}) hits in common, $(wc -l < {output.hgtector}) only found by hgtector\
 and $(wc -l < {output.indexes}) only found with the AIs"

cat {output.both} {output.hgtector} {output.indexes} > {output.all_ids}

cat {output.all_ids} | ./scripts/remove_inpara.R -p {input.paralogs} > {output.tree_ids} 

echo "!getting ids in paralogs clades just to test!"
shuf -n 400   {output.all_ids} > {output.tree_ids}
'''
# mkdir -p {output.plot}
# Rscript scripts/plot_results_HGT.R -i {input.hgtector}/scores.tsv -a {input.AI} -b {params.blast} \
# --both {output.both} --onlyai {output.indexes} --onlyhgtector {output.hgtector} -o {output.plot}

rule foldseek:
    input: 
        output+"{sp}/ids/{sp}_all.txt"
    output: 
        pdbs=directory(output+"{sp}/pdbs"),
        foldseek_tmp=temp(output+"{sp}/pdbs/{sp}_foldseek_out.tmp"),
        foldseek_out=output+"{sp}/pdbs/{sp}_foldseek_out.tsv",
        foldseek_html=output+"{sp}/pdbs/{sp}_foldseek_out.html"
    # benchmark:
    #     output+"benchmarks/{sp}_fs.txt"
    # params:
    #     tmpdir=temp(output+"{sp}/pdbs/tmp")
    threads:
        12
    shell:
        '''
python scripts/get_pdb.py -i {input} -o {output.pdbs}

foldseek easy-search {output.pdbs} {foldseek_db} {output.foldseek_tmp} {resources.tmpdir} --threads {threads} \
--format-output query,target,pident,alnlen,mismatch,gapopen,qstart,qend,tstart,tend,evalue,bits,qcov,tcov,taxid,lddt,alntmscore,rmsd,prob

cat {output.foldseek_tmp} | taxonkit reformat -I 15 -f \'{{k}}\t{{p}}\t{{c}}\t{{o}}\t{{f}}\t{{g}}\t{{s}}\' > {output.foldseek_out}

foldseek easy-search {output.pdbs} {foldseek_db} {output.foldseek_html} {resources.tmpdir} --threads {threads}\
 --format-mode 3
'''


# get fasta of filtered blast hits
rule get_fastas:
    input:
        prot=lambda wcs: mnemo_dict[wcs.sp]['proteome'],
        paralogs=rules.get_inparalogs.output,
        blast=rules.parse_blast.output
        # dummy=output+"{sp}/dummy/{seed}.txt"
    output:
        ids=output+"{sp}/seed_ids/{seed}.txt",
        fa=output+"{sp}/sequence/{seed}.fa",
        a=output+"{sp}/seed_ids/{seed}_self.txt",
        b=output+"{sp}/seed_ids/{seed}_close.txt",
        c=output+"{sp}/seed_ids/{seed}_others.txt",
        # tmp=temp(output+"{sp}/seed_ids/{sp}_tmpids.txt")
    params:
        # sdb=self_db+"{sp}",
        sub_db=lambda wcs: mnemo_dict[wcs.sp]['local_db'],
        taxid=lambda wcs: mnemo_dict[wcs.sp]['taxid']
    # benchmark:
    #     output+"benchmarks/{sp}_getfastas.txt"
    shell:'''
set +o pipefail;
mkdir -p $(dirname {output.ids})
mkdir -p $(dirname {output.fa})

grep "{wildcards.seed}" {input.paralogs} | tr '\\t' '\\n' | sort | uniq > {output.ids}

sort -u {output.ids} -o {output.a}
echo "Found $(wc -l < {output.a}) self"

seqkit grep -f {output.a} {input.prot} > {output.fa}

grep -f {output.ids} {input.blast} | cut -f2 | sort | uniq >> {output.ids}

sort -u {output.ids} -o {output.ids}

seqkit grep -f {output.ids} {params.sub_db} >> {output.fa}
seqkit seq -n -i {output.fa} | grep -v -f {output.a} > {output.b} || echo "No close hits found"

if [ ! -s "{output.b}" ]; then
    grep -v -f {output.a} {output.ids} | sort -u > {output.c}
else
    echo "Found $(wc -l < {output.b}) Close"
    grep -v -f {output.b} {output.ids} | grep -v -f {output.a} | sort -u > {output.c}
fi

blastdbcmd -db {db} -entry_batch {output.c} -logfile /dev/null >> {output.fa} || echo "Some sequences were not found"
echo "Found $(wc -l < {output.c}) others"
'''
# seqkit grep -f {output.ids} {params.sub_db} >> {output.fa}
# awk '$1=="{wildcards.seed}" && $11<1e-10' {input.selfblast} | cut -f2 \
# blastdbcmd -db {params.sdb} -entry_batch {output.a} > {output.fa}
# blastdbcmd -db {params.sdb} -entry {wildcards.seed} > {output.fa}


# align
rule famsa:
    input:
        rules.get_fastas.output.fa
    output:
        output+"{sp}/aligned/{seed}/{seed}_aln_famsa.fa"
    threads:
        12
    # benchmark:
    #     output+"benchmarks/{sp}_{seed}_famsa.txt"
    shell: "famsa -t {threads} {input} {output}"

rule mafft:
    input:
        output+"{sp}/sequence/{seed}.fa"
    output:
        output+"{sp}/aligned/{seed}/{seed}_aln_mafft.fa"
    # benchmark:
    #     output+"benchmarks/{sp}_mafft.txt"
    threads:
        12
    shell: "mafft --thread {threads} --auto {input} > {output}"

rule muscle:
    input:
        output+"{sp}/sequence/{seed}.fa"
    output:
        output+"{sp}/aligned/{seed}/{seed}_aln_muscle.fa"
    # benchmark:
    #     output+"benchmarks/{sp}_muscle.txt"
    threads:
        12
    shell: "muscle -align {input} -output {output} -threads {threads}"

rule kalign:
    input:
        output+"{sp}/sequence/{seed}.fa"
    output:
        output+"{sp}/aligned/{seed}/{seed}_aln_kalign.fa"
    # benchmark:
    #     output+"benchmarks/{sp}_kalign.txt"
    shell: "kalign -i {input} -o {output}"

# alns_method=['mafft', 'muscle', 'kalign']
alns_method=['famsa']


# rule mergealign:
#     input:
#         lambda wildcards: expand(output+"{sp}/aligned/{seed}/{seed}_aln_{method}.fa", 
#         sp=wildcards.sp, seed=wildcards.seed, method=alns_method)
#     output:
#         output+"{sp}/aligned/{seed}/{seed}_consensus.fa"
#     shell:
#         '''
# input_dir=$(dirname {output})
# cp {input} {output}
# '''
# python2 scripts/MergeAlign.py -a $input_dir -f {output}


# trim module
rule trim_aln:
    input:
        rules.famsa.output
    output:
        output+"{sp}/aligned/{seed}/{seed}_aln_trimmed.fa"
    # benchmark:
    #     output+"benchmarks/{sp}_trimal.txt"
    shell: "trimal -gappyout -in {input} -out {output}"

# tree reconstruction
rule tree:
    input:
        rules.trim_aln.output
    output:
        output+"{sp}/trees/{seed}.nwk"
    # benchmark:
    #     output+"benchmarks/{sp}_{seed}_ft.txt"
    shell: "FastTree {input} > {output}"
# -lg -gamma -cat 4 %s 

rule root_tree:
    input:
        rules.tree.output
    output:
        minvar=output+"{sp}/rooted_trees/rooted_{seed}_minvar.nwk",
        og=output+"{sp}/rooted_trees/rooted_{seed}_og.nwk",
        mp=output+"{sp}/rooted_trees/rooted_{seed}_midpoint.nwk",
        mad=output+"{sp}/rooted_trees/rooted_{seed}_mad.nwk"
    shell:'''
nleaves=$(tr -cd ',' < {input} | wc -c)
if [ $nleaves -lt 5 ]; then
    echo "too few leaves"
    touch {output}
else
    FastRoot.py -i {input} > {output.minvar}
    FastRoot.py -m MP -i {input} > {output.mp}
    Rscript scripts/root_furthest.R -i {input} -o {output.og}
    mad {input}
    mv {input}.rooted {output.mad}
fi
'''

# QC tree and alignment

# plot tree HGT
rule plot_tree:
    input:
        tree=output+"{sp}/rooted_trees/rooted_{seed}.nwk",
        aln=output+"{sp}/aligned/{seed}/{seed}_aln_famsa.fa",
    output:
        output+"{sp}/tree_plots/{seed}.png"
    params:
        tax=output+"{sp}/taxonomy/{sp}_tax.txt",
        self_seeds=output+"{sp}/seed_ids/{seed}_self.txt",
        taxid=lambda wcs:  mnemo_dict[wcs.sp]['taxid']
    shell:'''
Rscript scripts/plot_tree.R -i {input.tree} -a {input.aln} -o {output} \
 -t {params.tax} --selfid {params.taxid} -s {params.self_seeds}
'''

rule get_trees:
    input:
        trees=get_trees,
        # plots=get_tree_plots
    output:
        output+"{sp}/hgt_trees/{sp}_trees.nwk"
    shell: '''
for tree in {input.trees}; do
    gn=$(basename $tree ".nwk")
    echo -e "$gn\t$(cat $tree)" >> {output}
done
'''


rule get_rooted_trees:
    input:
        trees=get_rooted_trees,
        # plots=get_tree_plots
    output:
        output+"{sp}/hgt_trees/{sp}_rooted_trees.nwk"
    shell: '''
for tree in {input.trees}; do
    bn=$(basename $tree ".nwk" )
    rooting=$(echo $bn | sed 's/.*_//')
    gn=$(echo $bn |  sed 's/rooted_//' | rev | cut -f2- -d'_' | rev)

    echo -e "$gn\t$rooting\t$(cat $tree)" >> {output}
done
'''

rule plot_HGT_clusters:
    input:
        trees=rules.get_trees.output,
        tax=rules.get_taxonomy.output,
        both=rules.merge_IDs.output.both,
        hgtector=rules.merge_IDs.output.hgtector,
        indexes=rules.merge_IDs.output.indexes
    output:
        output+"{sp}/plots/{sp}_phylo_clusters.pdf"
    shell:'''
Rscript scripts/phylo_clusters.R -i {input.trees} -t {input.tax} -o {output} \
--onlyai {input.indexes} --onlyhgtector {input.hgtector} --both {input.both}
'''
