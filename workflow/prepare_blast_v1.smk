import pandas as pd

configfile: 'data/configs/blast_dbs.yml'

broaddb_taxdump = config['taxdump']

input_data = pd.read_csv(config['input'], sep='\t')
clades = list(set(input_data['clade']))

clade_dict = input_data.set_index('clade').T.to_dict()

rule all:
    input:
        expand(config['sub_db']+'{clade}/{clade}_proteomes.dmnd', clade=clades),
        expand(config['sub_db']+'{clade}/{clade}_taxdump', clade=clades)


# The user can look either with a custom db or look for a taxid and get the annotated genomes of that taxid.
# If type is ncbi do it otherwise use the one give by the user

rule get_genomes:
    params:
        mode=lambda wcs: clade_dict[wcs.clade]['mode'],
        db=lambda wcs: clade_dict[wcs.clade]['db'],
        lng=lambda wcs: clade_dict[wcs.clade]['lng']
    output:
        folder=temp(directory(config['sub_db']+'{clade}/raw/')),
        meta=config['sub_db']+'{clade}/{clade}_metadata.tsv',
    wildcard_constraints:
        mode="ncbi"
    shell:'''

mkdir -p {output.folder}

if [ "{params.mode}" == "ncbi" ]; then

    datasets summary genome taxon {wildcards.clade} --annotated --as-json-lines | \
    jq -r '. | [.accession, .organism.tax_id, .annotation_info.stats.gene_counts.protein_coding, .assembly_info.assembly_level, \
    .assembly_stats.contig_l50, .assembly_stats.contig_n50, .assembly_stats.gc_percent, .assembly_stats.number_of_contigs, \
    .assembly_stats.total_sequence_length, .organism.organism_name] | @tsv' | uniq -f1 |
    taxonkit reformat -I2 > {output.meta}

for query in $(cut -f1 {output.meta}); do
    echo $query
    datasets download genome accession $query \
    --include protein --filename {output.folder}/${{query}}.zip
    unzip -j {output.folder}/${{query}}.zip '*protein.faa' -d {output.folder}
    mv {output.folder}/protein.faa {output.folder}/${{query}}.faa
    rm {output.folder}/${{query}}.zip
done

elif [ "{params.mode}" == "custom" ]; then
    cp {params.db}/* {output.folder}
    cp {params.lng} {output.meta}
fi 
'''

rule get_new_taxdump:
    input: 
        meta=rules.get_genomes.output.meta,
        tsv=config['tax_tsv']
    output: 
        lineage=config['sub_db']+'{clade}/{clade}_lineage.tsv',
        taxdump=directory(config['sub_db']+'{clade}/{clade}_taxdump'),
        new_ids=config['sub_db']+'{clade}/{clade}_taxdump/{clade}.ids'
    shell:'''
mkdir -p {output.taxdump}
cut -f1,2 {input.meta} | taxonkit lineage -i2 | ./scripts/get_tax.R | python scripts/fill_na.py > {output.lineage}
cp {broaddb_taxdump}/*dmp {output.taxdump}
python scripts/update_tax.py -i {output.lineage} -o {output.taxdump} -m {output.new_ids}
'''
# | python ./scripts/fill_na.py
rule parse_fastas:
    input: 
        folder=rules.get_genomes.output.folder,
        meta=rules.get_new_taxdump.output.new_ids
    output:
        fa=config['sub_db']+'{clade}/{clade}.fa',
        id_map=config['sub_db']+'{clade}/{clade}.map',
        # blast=config['sub_db']+'{clade}/blast_db/{clade}_proteomes.pdb'
        # blast=multiext(config['sub_db']+'{clade}/blast_db/{clade}_proteomes',
        #     ".pdb",".phr",".pin",".pot",".psq",".ptf",".pto")
    shell:'''
parsed_dir=$(dirname {output.fa})
mkdir -p $parsed_dir/parsed
python scripts/parse_gnm.py -i {input.meta} -d {input.folder} -o $parsed_dir/parsed
cat $parsed_dir/parsed/*fa > {output.fa}
grep '>' {output.fa} | sed 's/>//' | awk 'BEGIN {{FS=OFS="_"}} {{print $0"\t"$1}}'  > {output.id_map}
rm -r $parsed_dir/parsed
'''
# ob={output.blast}
# output_blast="${{ob%%.*}}"
# makeblastdb -in {output.fa} -out $output_blast -dbtype prot -taxid_map {output.id_map} -parse_seqids

rule make_diamond:
    input: rules.parse_fastas.output.fa,
    output:
        dmnd=config['sub_db']+'{clade}/{clade}_proteomes.dmnd'
        # blast=multiext(config['sub_db']+'{clade}/blast_db/{clade}_proteomes',
        #     ".pdb",".phr",".pin",".pot",".psq",".ptf",".pto")
    shell:'''
ob={output.dmnd}
output_dmnd="${{ob%%.*}}"
diamond makedb --in {input} -d $output_dmnd
'''
# CHECK IF PARSING IS DOING WHAT YOU WANT THEN DELETE ALL AND RERUN!

# TODO:
# see scripts/plot_subdb.R to quickly explore the db 
